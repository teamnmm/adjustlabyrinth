class Block extends BaseCharacter {

  Block( MyGame parent, float x, float y, int size ) {
    super( parent );
    _x = x;
    _y = y;
    _size = size;
    
    _name = "Block";
  }

  public void update() {
    super.update();
  }

  void draw() {
    fill( 0, 255, 0 );
    rect( _x - _size / 2, _y - _size / 2, _size, _size );
  }
}

