class Enemy extends BaseCharacter {
  
  public static final int JUMP_MAX = 20;
  private int _jumpSpeed;
  private int _jumpCount;
  private boolean _up, _right, _left, _down;
  private boolean _upEnd;
  private int _wallSize;
  private boolean _isDead;
  private int longitudinalSpeed;
  private boolean moveLeft;

  Enemy( MyGame parent, int x, int y ) {
    super( parent );
    _x = x;
    _y = y;
    _size = 30;
    _wallSize = 40;
    _isDead = false;
    _upEnd = false;
    moveLeft = false;
    _speed = longitudinalSpeed = 1;
  }

  void update() {
    if ( !_isDead ) {
      fill( 0 );
      textSize( 20 );
      textAlign( CENTER );
      text( "ENEMY", _x, _y + _size );
      fill( 255, 0, 255 );
      ellipse( _x, _y, _size, _size );
      move();
      if( hitTest( _parent._kPlayer, _size )){
        _parent._state = _parent.RESULT_SCREEN;
      }
      super.update();
    }else{
      _x = width / 2;
      _y = height + 200;
    }
  }
  
  
  private void move() {
    int dx01 = (int)((_x + _size ) / _wallSize );
    int dy01 = (int)(_y / _wallSize );
    if ( !moveLeft ) {
      if ( _x < width - _size  ) {
        if ( _parent.theMap[dy01][dx01] == 0 ) {
          _x += _speed;
        } else if ( _parent.theMap[dy01][dx01] == 1 ) {
          _speed = _speed * - 1;
          moveLeft = true;
        }
      } else {
        _speed = _speed * - 1;
        moveLeft = true;
      }
    }

    int dx02 = (int)((_x - _size ) / _wallSize);
    int dy02 = (int)(_y / _wallSize );
    if ( moveLeft ) {
      if ( _x > _size ) {
        if ( _parent.theMap[dy02][dx02] == 0 ) {
          _x += _speed;
        } else if ( _parent.theMap[dy02][dx02] == 1 ) {
          _speed = _speed * - 1;
          moveLeft = false;
        }
      } else {
        _speed = _speed * - 1;
        moveLeft = false;
      }
    }

    int dx03 = (int)(_x / _wallSize);
    int dy03 = (int)((_y + _size ) / _wallSize );
    if ( _parent.theMap[dy03][dx03] == 0 ) {
      _y += longitudinalSpeed;
    }
  }
}

