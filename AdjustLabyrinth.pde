MyGame _game;

int LENGTH;
int [][] csv;

void setup(){
  size( 640, 480 );
  loadCsvFile();
  _game = new MyGame();
  _game.init();
  
  noCursor();
}
void draw(){
  background( 255 );
  _game.update();
}

void keyPressed() {
  switch(keyCode) {
  case RIGHT:
    _game._kPlayer._right = true;
    break;
  case LEFT:
    _game._kPlayer._left = true;
    break;
  case UP:
    _game._kPlayer._up = true;
    break;
  case DOWN:
    _game._kPlayer._down = true;
    break;
  }
}
void keyReleased() {
  switch(keyCode) {
  case RIGHT:
    _game._kPlayer._right = false;
    break;
  case LEFT:
    _game._kPlayer._left = false;
    break;
  case UP:
    _game._kPlayer._up = false;
    break;
  case DOWN:
    _game._kPlayer._down = false;
    break;
  }
}

void loadCsvFile(){
  //csv process
  int csvWidth=0;
  String lines[] = loadStrings("map.csv");
  for (int i=0; i < lines.length; i++) { 
    String [] chars=split(lines[i],','); 
    if (chars.length>csvWidth){
      csvWidth=chars.length;
    }
  }
  csv = new int [lines.length][csvWidth];
  LENGTH =lines.length; 
  for (int i=0; i < lines.length; i++) {
    String [] temp = new String [lines.length];
    temp= split(lines[i], ',');
    for (int j=0; j < temp.length; j++){
      csv[i][j]=int(temp[j]);
    }
  }
}
