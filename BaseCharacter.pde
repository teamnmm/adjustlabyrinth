class BaseCharacter{
  // ---- Field ----
  protected MyGame _parent;
  protected float _x, _y;
  protected int _size, _speed;
  protected static final float GRAVITY = 9.8;
  protected static final float GRAVITY_DECELERATION = 0.01;//= 0.75;
  protected static final float SCROLL_SPEED = 5;
  protected float _gravitySpeed;
  protected int _screenY;
  
  protected String _name;
  
  // ---- Constracter ----
  BaseCharacter( MyGame parent ){
    _parent = parent;
    _screenY = height;
  }
  
  //---- Initiarize ----
  public void init(){
  
  }
  
  // --- Update ---
  public void update(){
    draw();
  }
  
  protected void draw(){
    //viewName();
  }
  
  private void viewName(){
    textSize( 20 );
    fill( 0 );
    textAlign( CENTER );
    text( _name, _x, _y + _size );
  }
  
  // ---- HitTest ----
  protected boolean hitTest( BaseCharacter target, float distance ){
    if( dist( _x, _y, target._x, target._y ) <= distance ){
      return true;
    }else{
      return false;
    }
  }
  
  void scrollCharacter(){
    _x -= SCROLL_SPEED;
  }
  
  public float getPosX(){
    return _x;
  }
  
  public float getPosY(){
    return _y;
  }
  
  public float getObjSize(){
    return _size;
  }
}
