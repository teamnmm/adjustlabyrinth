class MyGame {

  ArrayList<BaseCharacter> _blocks;
  ArrayList<BaseCharacter> _allCharacters;
  MousePlayer _mPlayer;
  KeyPlayer _kPlayer;
  Wall[] _wall;
  Enemy _enemy;

  int WALL_MAX;
  static final int TITLE_SCREEN = 0;
  static final int GAME_SCREEN = 1;
  static final int RESULT_SCREEN = 2;
  static final int BLOCK_MAX = 100;
  static final int BLOCK_SIZE = 40;

  private int _state;
  private int wall_count;
  private int _blockX, _blockY;
  private Boolean block_aruyoo, block_make;
  private Boolean mblock_aruyoo; 

  public int[][] theMap;

  //access property
  public ArrayList<BaseCharacter> getBlocks() {
    return _blocks;
  }

  public ArrayList<BaseCharacter> getAllCharacters() {
    return _allCharacters;
  }

  public Wall[] getWalls() {
    return _wall;
  }

  // --- Constracter ----
  MyGame() {
    theMap = csv;
    WALL_MAX = theMap[0].length * theMap.length;
  }

  // ---- init -----
  void init() {
    _state = TITLE_SCREEN;
    wall_count = 0;
    block_aruyoo = false;
    mblock_aruyoo = false;
    _blocks = new ArrayList<BaseCharacter>();
    _allCharacters = new ArrayList<BaseCharacter>();
    //_enemy = new Enemy( this, width / 2, height / 2 );
    _mPlayer = new MousePlayer( this );
    _kPlayer = new KeyPlayer( this, width/2, 0 );
    //_allCharacters.add(_enemy);
    //_allCharacters.add(_mPlayer);
    //_allCharacters.add(_kPlayer);
    _wall = new Wall[ WALL_MAX ];
    for ( int i = 0; i < theMap.length; i++ ) {
      for ( int j = 0; j < theMap[i].length; j++ ) {
        if ( theMap[i][j] == 1 ) {
          _wall[wall_count] = new Wall( this, j, i );
          _allCharacters.add(_wall[wall_count]);
        } else {
          _wall[wall_count] = null;
        }
        wall_count = wall_count + 1;
      }
    }
  }

  void update() {
    switch( _state ) {
    case TITLE_SCREEN:
      _state = title();
      break;
    case GAME_SCREEN:
      _state = game();
      break;
    case RESULT_SCREEN:
      _state = result();
      break;
    }
  }

  // --- state method start ---

  // ------ TITLE --------
  private int title() {
    int result = _state;
    fill( 0 );
    textAlign( CENTER );
    textSize( 50 );
    text( "Adjust Labyrinth\nGo Right to Game Start", width / 2, height / 4 );
    updateCharacters();
    if ( _kPlayer.getStartGame() ) {
      result = GAME_SCREEN;
    }
    return result;
  }

  // ----- GAME ----------
  private int game() {
    int result = _state;
    updateCharacters();
    return result;
  }

  // ----- RESULT -------
  private int result() {
    int result = _state;
    
    return result;
  }

  // --- state method end ---

  public void instanceBlock(BaseCharacter block) {
    _blocks.add(block);
    _allCharacters.add(block);
  }
  
  private void updateCharacters(){
    for ( Wall theWall : _wall ) {
      if ( theWall != null ) {
        theWall.update();
      }
    }
    // --- BLOCK MAKE ---
    //after add block
    for (BaseCharacter block : _blocks) {
      block.update();
    }
    // ---     ---
    _mPlayer.update();
    _kPlayer.update();
  }

  public void changeBackground() {
    for ( Wall theWall : _wall ) {
      if ( theWall != null ) {
        theWall.scrollCharacter();
      }
    }
    for (BaseCharacter block : _blocks) {
      block.scrollCharacter();
    }
  }
}

