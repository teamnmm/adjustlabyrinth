class MoveBlock extends Block {
  static final int FALL = 0;
  static final int WAITS = 1;
  private int _state;

  // Constracter
  MoveBlock( MyGame parent, float x, float y, int size ) {
    super( parent, x, y, size );
    _x = x;
    _y = y;
    _size = size;
    _gravitySpeed = 0;
    _state = FALL;
    
    _name = "MoveBlock";
  }

  // Update
  public void update() {
    super.update();
    switch( _state ) {
    case FALL:
      _gravitySpeed += GRAVITY/60;
      _y += _gravitySpeed;

      if ( hitTest( _parent._kPlayer, _size ) ) {
        _parent._kPlayer.damageHP();
      } 
      
      for(BaseCharacter block : _parent.getBlocks()){
        if(block != this && hitTest(block, _size)){
          _y = block.getPosY() - _size;// / 2;
          _state = WAITS;
        }
      }
      
      for(BaseCharacter wall : _parent.getWalls()){
        if(wall != null && wall != this && hitTest(wall, _size)){
          _y = wall.getPosY() - _size;
          _state = WAITS;
        }
      }
      break;
    case WAITS:
    
      break;
    }
  }
  
  void draw(){
    super.draw();
    fill( 255, 0, 0 );
  }
}

