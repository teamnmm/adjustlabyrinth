class Wall extends BaseCharacter{
  public boolean hit = false;
  public boolean down = false;
  private int _dx, _dy;
  
  Wall( MyGame parent, int dx, int dy ){
    super( parent );
    _dx = dx;
    _dy = dy;
    _size = 40;
    _x = ( _dx * _size ) + _size / 2;
    _y = ( _dy * _size ) + _size / 2;
  }
  
  void update(){
    //_x -= SCROLL_SPEED;
    super.update();
  }
  
  void draw(){
    fill( 255, 180, 0 );
    rect( _x - _size / 2, _y - _size / 2, _size , _size );
  }
}
