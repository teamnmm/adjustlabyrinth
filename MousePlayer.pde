class MousePlayer extends BaseCharacter {

  private int make_count;
  private boolean block_make;
  
  // Constracter
  MousePlayer( MyGame parent ) {
    super( parent );
    _size = 30;
    block_make = true;
    make_count = 0;
    _name = "MousePlayer";
  }
  
  // Update
  public void update() {
    _x = mouseX;
    _y = mouseY;
    instanceAction();
    waitInstance();
    super.update();
  }
  
  void draw(){
    super.draw();
    fill( 255, 0, 0 );
    ellipse( mouseX, mouseY, _size, _size );
  }
  
  private void instanceAction(){
    if ( mousePressed && block_make ) {
      if ( mouseButton == RIGHT ) {
        _parent.instanceBlock(new Block(_parent, _x, _y, _parent.BLOCK_SIZE));
      }
      if ( mouseButton == LEFT ) {
        _parent.instanceBlock(new MoveBlock(_parent, _x, _y, _parent.BLOCK_SIZE));
      }
      block_make = false;
    }
  }
  
  private void waitInstance(){
    if ( !block_make ) {
      make_count++;
      if ( make_count > 120 ) {
        block_make = true;
        make_count = 0;
      }
    }
  }
}

