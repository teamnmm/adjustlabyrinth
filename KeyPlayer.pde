class KeyPlayer extends BaseCharacter {

  private final int SIDE_LEFT = 0;
  private final int SIDE_RIGHT = 1;

  public boolean _up, _down, _left, _right;

  private int _hp;
  private boolean _isDead;
  private boolean _resetPosition;
  private boolean _startGame;
  private boolean _jumpping;
  private boolean _fall;
  private float _speed;
  private float _keepY;

  private BaseCharacter _underObj;
  private BaseCharacter[] _sideObj;

  KeyPlayer( MyGame parent, int x, int y ) {
    super( parent );
    _hp = 3;
    _x = x;
    _y = y;
    _size = 30;
    _isDead = false;
    _speed = 3;
    _resetPosition = false;
    _startGame = false;
    _underObj = null;
    _sideObj = new BaseCharacter[2];
    _sideObj[SIDE_LEFT] = null;
    _sideObj[SIDE_RIGHT] = null;  
    _speed = 3;
    _jumpping = false;
    _name = "KeyPlayer";
    _fall = false;
  }

  public void update() {
    if ( !_isDead ) {
      super.update();

      //damage process
      if (_y > height + _size) {
        damageHP();
      }

      // --- next stage --- start
      if (_x > width - _size * 2) {
        _resetPosition = true;
      }
      if (_resetPosition) {
        _parent.changeBackground();
        _startGame = true;
        scrollCharacter();
        if (_x < 0 + _size * 2) {
          _resetPosition = false;
        }
        super.update();
        return;
      }
      // --- next stage --- end

      controll();

      sideHit();

      jumpping();

      if (underHit()) return;
      
      gravityFall();
    } else {
      _parent._state = _parent.RESULT_SCREEN;
    }
    
    println(_jumpping + "  " + _fall);
  }

  void draw() {
    super.draw();
    fill( 255, 255, 0 );
    for (int i = 0; i<_hp; i++) {
      rect( _size * (i + 1), 10, _size/2, _size/2);
    }
    ellipse( _x, _y, _size, _size );
  }

  public void damageHP() {
    if (_startGame) {
      _hp -= 1;
      _x = 0 + _size;
      _y = height/2;
    } else {
      _x = width/2;
      _y = 0;
    }
    _underObj = null;
    _sideObj[SIDE_LEFT] = null;
    _sideObj[SIDE_RIGHT] = null;
    _gravitySpeed = 0;
    if (_hp == 0) _isDead = true;
  }

  public boolean getStartGame() {
    return _startGame;
  }

  private void controll() {
    if (_left && _x > 0) {
      _x -= _speed;
    }
    if (_right && _x < width) {
      _x += _speed;
    }
    if(_up){
      if(!_jumpping && !_fall){
        _keepY = _y;
      }
      _jumpping = true;
    }
  }
  
  private void jumpping(){
    if(_jumpping && !_fall){
      _underObj = null;
      _sideObj[SIDE_LEFT] = null;
      _sideObj[SIDE_RIGHT] = null;
      _gravitySpeed += GRAVITY/60;
      _y -= _gravitySpeed;
    }
    
    if(_keepY - 50 > _y){
      _fall = true;
    }else{
      if(upHit()){
        _fall = true;
      }
    }
    
  }

  private void gravityFall() {
    _gravitySpeed += GRAVITY/60;
    _y += _gravitySpeed;
    if(_y >= height){
      _underObj = null;
      _sideObj[SIDE_LEFT] = null;
      _sideObj[SIDE_RIGHT] = null;
    }
  }

  private boolean underHit() {
    if(_jumpping && !_fall){
      return true;
    }
    
    if (_underObj != null) {
      if(_underObj.getPosX() + _underObj.getObjSize() / 2 < _x - _size / 4 || 
          _underObj.getPosX() - _underObj.getObjSize() / 2 > _x + _size / 4 || _underObj.getPosY() + _underObj.getObjSize() / 2 < _y - _size / 4){
        _underObj = null;
        return false;
      }
      _y = _underObj.getPosY() - _size;
      _gravitySpeed = 0;
      _fall = false;
      _jumpping = false;
      return true;
    }

    for (BaseCharacter obj : _parent.getAllCharacters ()) {
      if (hitTest(obj, _size)) {
        _underObj = obj;
        return true;
      }
    }
    return false;
  }
  
  private void sideHit() {
    if (_left && _sideObj[SIDE_LEFT] != null) {
      if( _sideObj[SIDE_LEFT].getPosX() > _x - _size) _x += _speed;
    }
    if (_right && _sideObj[SIDE_RIGHT] != null) {
      if(_sideObj[SIDE_RIGHT].getPosX() < _x + _size) _x -= _speed;
    }

    for (BaseCharacter obj : _parent.getAllCharacters ()) {
      if (obj.getPosY() <= _y && (_underObj != null || _underObj != obj)) {
        //left
        if (hitTest(obj, _size) && obj.getPosX() < _x - _size/2) {
          _sideObj[SIDE_LEFT] = obj;
        }
        //right
        if (hitTest(obj, _size) && obj.getPosX() > _x + _size/2) {
          _sideObj[SIDE_RIGHT] = obj;
        }
      }
    }
  }
  
  private boolean upHit(){
    for (BaseCharacter obj : _parent.getAllCharacters ()) {
      if (hitTest(obj, _size) && obj.getPosY() < _y - _size/2) {
        return true;
      }
    }
    return false;
  }
}

